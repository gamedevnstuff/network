// Generated by CoffeeScript 1.9.3
var append_event, connected, connection, events, incoming_events, key, onchallenge, process_incoming, protocol, s, send, tick, user, wsuri;

console.log("Ok, AutobahnJS loaded", autobahn.version);

user = "joe";

key = "123";

connected = false;

s = false;

events = [];

incoming_events = [];

onchallenge = function(session, method, extra) {
  if (method === "wampcra") {
    console.log("onchallenge: authenticating via '" + method + "' and challenge '" + extra.challenge + "'");
    return autobahn.auth_cra.sign(key, extra.challenge);
  } else {
    throw "don't know how to authenticate using '" + method + "'";
  }
};

wsuri = '';

if (document.location.origin === "file://") {
  wsuri = "ws://127.0.0.1:8080/ws";
} else {
  if (document.location.protocol === "http:") {
    protocol = 'ws:';
  } else {
    protocol = 'wss:';
  }
  wsuri = protocol + "//" + document.location.host + "/ws";
}

connection = new autobahn.Connection({
  url: wsuri,
  realm: 'realm1',
  authmethods: ["wampcra"],
  authid: user,
  onchallenge: onchallenge
});

connection.onopen = function(session, details) {
  console.log("connected session with ID " + session.id);
  console.log("authenticated using method '" + details.authmethod + "' and provider '" + details.authprovider + "'");
  console.log("authenticated with authid '" + details.authid + "' and authrole '" + details.authrole + "'");
  connected = true;
  s = session;
  console.log(session.subscribe('com.game.rooms.0.0', function(args, kwargs, details) {
    incoming_events.push({
      args: args,
      kwargs: kwargs,
      details: details
    });
  }));
};

connection.onclose = function(reason, details) {
  console.log("disconnected", reason, details.reason, details);
  connected = false;
};

send = function(event_dict) {
  var events_topic;
  events_topic = 'com.game.events.' + user;
  console.log('sending ' + event_dict + ' to ' + events_topic);
  if (connected) {
    console.log();
    s.publish(events_topic, [event_dict], {}, {
      acknowledge: true
    });
  } else {
    console.log('not connected...');
  }
};

console.log('connecting to ' + wsuri);

connection.open();

append_event = function(event) {
  return events.push(event);
};

process_incoming = function() {
  var event, i, len;
  for (i = 0, len = incoming_events.length; i < len; i++) {
    event = incoming_events[i];
    console.log(event);
    if (event['args'][0]['t'] === 'draw') {
      draw(event['args'][0]['coords']['x'], event['args'][0]['coords']['y']);
    }
  }
  return incoming_events = [];
};

tick = function() {
  var event, i, len;
  if (events.length !== 0) {
    console.log('sending ' + events.length + ' events');
  }
  for (i = 0, len = events.length; i < len; i++) {
    event = events[i];
    send(event);
  }
  events = [];
  process_incoming();
  return setTimeout(tick, 16);
};

tick();

//# sourceMappingURL=network.js.map
